﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMountain : MonoBehaviour
{
    public CameraShake cameraShake;
    public void OnTriggerEnter(Collider abc)
    {
        if (abc.CompareTag("Mountain"))
        {
            AudioSource crack = GetComponent<AudioSource>();
            crack.Play();
        }
    }
}
