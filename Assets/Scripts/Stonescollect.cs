﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stonescollect : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {

        if (collision.CompareTag("Player"))
        {
            
            FindObjectOfType<UIStones>().AdvancePoints();
            
        }
        
    }

}
