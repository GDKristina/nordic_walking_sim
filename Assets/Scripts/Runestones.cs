﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]

public class Runestones : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {            
            AudioSource voice = GetComponent<AudioSource>();
            voice.Play();
            //FindObjectOfType<UIStones>().AdvancePoints();
        }
    }
}

