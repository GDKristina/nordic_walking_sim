﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerShake : MonoBehaviour
{
    public CameraShake cameraShake;
    public void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            StartCoroutine(cameraShake.Shake(.05f, .2f));
            AudioSource boom = GetComponent<AudioSource>();
            boom.Play();
        }
    }
}