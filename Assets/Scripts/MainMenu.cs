﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }


    public void ButtonPlay()
    {
        SceneManager.LoadScene("Beginning");
        Cursor.visible = false;
    }

    public void ButtonCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void ButtonQuit()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
