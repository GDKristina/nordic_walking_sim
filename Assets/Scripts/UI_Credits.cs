﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UI_Credits : MonoBehaviour
{
    public CanvasGroup canvas;

    public GameObject panel;

    public Animation anim;

    protected virtual void Awake()
    {
        canvas = GetComponent<CanvasGroup>();
    }
     
    public void Show()
    {
        canvas.alpha = 1f;
        canvas.blocksRaycasts = true;
        canvas.interactable = true;
        anim = panel.GetComponent<Animation>();
        anim.Play();
    }

    public void Hide()
    {
        canvas.alpha = 0f;
        canvas.blocksRaycasts = false;
        canvas.interactable = false;
    }
}
