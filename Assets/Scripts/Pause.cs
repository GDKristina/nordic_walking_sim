﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Pause : UI_Pause
{
    private void Awake()
    {
        Hide();
    }


    void Update()
    {
        var FirstPersonController = GetComponentInParent<CharacterController>();



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            var stun = FirstPersonController.GetComponent<FirstPersonController>();
            stun.enabled = false;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            Show();
        }
        else
        {
            Hide();
        }
    }
}
