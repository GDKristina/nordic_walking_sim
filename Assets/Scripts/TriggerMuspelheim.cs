﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerMuspelheim : MonoBehaviour
{
    void OnTriggerEnter(Collider musp)
    {
        if (musp.CompareTag("Player"))
        {
            SceneManager.LoadScene("Muspelheim");
        }
    }
}
