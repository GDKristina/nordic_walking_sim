﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class UI_Pause : MonoBehaviour
{
    public CanvasGroup canvas;

    public GameObject panel;

    protected virtual void Awake()
    {
        canvas = GetComponent<CanvasGroup>();
    }

    public void Show()
    {
        canvas.alpha = 1f;
        canvas.blocksRaycasts = true;
        canvas.interactable = true;
    }

    public void Hide()
    {
        canvas.alpha = 0f;
        canvas.blocksRaycasts = false;
        canvas.interactable = false;
    }
}
