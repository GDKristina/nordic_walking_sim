﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerMidgard : MonoBehaviour
{
    void OnTriggerEnter(Collider midg)
    {
        if (midg.CompareTag("Player"))
        {
            SceneManager.LoadScene("Midgard");
        }
    }
}
