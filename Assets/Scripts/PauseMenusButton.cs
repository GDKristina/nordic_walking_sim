﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenusButton : MonoBehaviour
{

 


    public void StartAgainButton()
    {
        SceneManager.LoadScene("Beginning");
        Time.timeScale = 1f;
        Debug.Log("time");
        AudioListener.pause = false;
    }

    public void MenuButton()
    {
        SceneManager.LoadScene("TitleMenu");
        Time.timeScale = 1f;
        AudioListener.pause = false;
    }

    public void QuitButton()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
