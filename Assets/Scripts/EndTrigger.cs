﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class EndTrigger : UI_Credits
{
    private void Awake()
    {
        Hide();
    }


    void OnTriggerEnter(Collider other)
    {
        var FirstPersonController = other.GetComponentInParent<CharacterController>();
        
        if (FirstPersonController != null)
        {
            var stun = FirstPersonController.GetComponent<FirstPersonController>();            
            stun.enabled = false;            
            
            AudioSource voice = GetComponent<AudioSource>();
            voice.Play();

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            
            Show();
        }
    }
}
