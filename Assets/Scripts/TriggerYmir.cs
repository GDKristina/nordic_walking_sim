﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerYmir : MonoBehaviour
{
    public CameraShake cameraShake;
    public void OnTriggerEnter(Collider abc)
    {
        if (abc.CompareTag("Ymir"))
        {
            StartCoroutine(cameraShake.Shake(.15f, .4f));
            AudioSource boom = GetComponent<AudioSource>();
            boom.Play();
        }
    }
}
