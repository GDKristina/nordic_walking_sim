﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CanvasGroup))]
public class UIScene : MonoBehaviour
{
    private CanvasGroup canvas;

    protected virtual void Awake()
    {
        canvas = GetComponent<CanvasGroup>();
        //Hide();
    }

    public void Show()
    {
        canvas.alpha = 1f;
        canvas.blocksRaycasts = true;
        canvas.interactable = true;
        //Debug.log
    }

    //public void Hide()
    //{
    //    canvas.alpha = 0f;
    //    canvas.blocksRaycasts = false;
    //    canvas.interactable = false;
    //}

}
