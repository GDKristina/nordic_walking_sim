﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStones : UIScene
{

    private Text label;

    private /*static*/ int score = 0;

    protected override void Awake()
    {
        base.Awake();

        label = GetComponent<Text>();
    }

    public void AdvancePoints()
    {
        if (score <1)
        {
            score++;
            label.text = score + "/1";
        }
       
    }

    //public void Start()
    //{
    //    Hide();
    //}

    public void ResetScore()
    {
        score = 0;
        label.text = score + "/1 ";
    }
}
