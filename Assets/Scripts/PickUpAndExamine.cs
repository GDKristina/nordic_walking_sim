using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


public class PickUpAndExamine : MonoBehaviour
{

	private GameObject hitObject;
	private bool objectIsPickedUp = false;
	private bool examiningObject = false;

	private FirstPersonController myFirstPersonController;

	public GameObject handPosition;
	public GameObject examinePosition;

	public float thrust = 300f;

	public float zoomFOV = 30.0f;
	public float zoomSpeed = 9f;

	private float targetFOV;
	private float baseFOV;
    
	void Start ()
	{
		SetBaseFOV(GetComponent<Camera>().fieldOfView);
        
		myFirstPersonController = transform.parent.gameObject.GetComponent<FirstPersonController>();
        

	}
    
	void Update()
	{
        if (Input.GetButton("Fire1"))
        {
            targetFOV = zoomFOV;
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;
            Physics.Raycast(ray, out hit);
        }

        else
        {
            targetFOV = baseFOV;
        }

        UpdateZoom();

    }

	private void UpdateZoom()
	{
		GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, targetFOV, zoomSpeed * Time.deltaTime);
	}

	public void SetBaseFOV(float fov)
	{
		baseFOV = fov;
	}
}
