﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(AudioSource))]
public class PauseTest : MonoBehaviour
{
    public Transform canvas;
    public Transform Player;

    void Awake()
    {
        canvas.gameObject.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;
        Debug.Log("curstrue");
    }


    void Update()
    {
      
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(canvas.gameObject.activeInHierarchy == false)
            {
                Debug.Log("canvastrue");
                canvas.gameObject.SetActive(true);
                Time.timeScale = 0f;
                Player.GetComponent<FirstPersonController>().enabled = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                AudioListener.pause = true;

               
            }
            else
            {
                Debug.Log("canvasfalse");
                canvas.gameObject.SetActive(false);
                Time.timeScale = 1f;
                Player.GetComponent<FirstPersonController>().enabled = true;
                AudioListener.pause = false;
                Cursor.visible = false;

            }
        }
    }


}
